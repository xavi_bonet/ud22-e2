![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD22 - E2 / MVC

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Team Member | 01/02/2021 |   |   |  |
| David Bonet Daga | Master | Team Member | 01/02/2021 |   |   |  |

#### 2. Description
```
Ejercicio MVC
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/ud22-e2.git

```
UD22 - E2 - MVC / https://gitlab.com/xavi_bonet/ud22-e2.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
Apache Maven - http://maven.apache.org/download.cgi
JUnit
IDE - Eclipse Enterprise
```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.
