package c4.ud22_patronMVC.AppMain;

import c4.ud22_patronMVC.controller.ClienteController;
import c4.ud22_patronMVC.controller.VideoController;
import c4.ud22_patronMVC.model.service.ClienteServ;
import c4.ud22_patronMVC.model.service.VideoServ;
import c4.ud22_patronMVC.view.VentanaBuscar;
import c4.ud22_patronMVC.view.VentanaBuscarVideo;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.VentanaRegistro;
import c4.ud22_patronMVC.view.VentanaRegistroVideo;
import c4.ud22_patronMVC.view.VentanaListar;
import c4.ud22_patronMVC.view.VentanaListarVideo;

public class mainApp {
	
	ClienteServ miclienteServ;
	VideoServ mivideoServ;
	VentanaPrincipal miVentanaPrincipal;
	VentanaBuscar miVentanaBuscar;
	VentanaBuscarVideo miVentanaBuscarVideo;
	VentanaRegistro miVentanaRegistro;
	VentanaRegistroVideo miVentanaRegistroVideo;
	ClienteController clienteController;
	VideoController videoController;
	
	//Ventanas Listar
	VentanaListar miVentanaListar;
	VentanaListarVideo miVentanaListarVideo;

	public static void main(String[] args) {
		mainApp miPrincipal=new mainApp();
		miPrincipal.iniciar();
	}

	/**
	 * Permite instanciar todas las clases con las que trabaja
	 * el sistema
	 */
	private void iniciar() {
		/*Se instancian las clases*/
		miVentanaPrincipal=new VentanaPrincipal();
		miVentanaRegistro=new VentanaRegistro();
		miVentanaRegistroVideo=new VentanaRegistroVideo();
		miVentanaBuscar= new VentanaBuscar();
		miVentanaBuscarVideo= new VentanaBuscarVideo();
		miclienteServ=new ClienteServ();
		mivideoServ=new VideoServ();
		clienteController= new ClienteController();
		videoController= new VideoController();
		
		//Ventana Listar
		miVentanaListar=new VentanaListar();
		miVentanaListarVideo=new VentanaListarVideo();
		
		
		
		/*Se establecen las relaciones entre clases*/
		//Ventana principal
		miVentanaPrincipal.setCoordinador(clienteController, videoController);
		
		//Cliente
		miVentanaRegistro.setCoordinador(clienteController);
		miVentanaBuscar.setCoordinador(clienteController);
		miVentanaListar.setCoordinador(clienteController);
		
		miclienteServ.setpersonaController(clienteController);
		
		
		//Video
		miVentanaRegistroVideo.setCoordinador(clienteController, videoController);
		miVentanaBuscarVideo.setCoordinador(videoController);
		miVentanaListarVideo.setCoordinador(videoController);
		
		mivideoServ.setpersonaController(videoController);
		
		
		
		
		/*Se establecen relaciones con la clase coordinador*/
		//Cliente
		clienteController.setMiVentanaPrincipal(miVentanaPrincipal);
		clienteController.setMiVentanaRegistro(miVentanaRegistro);
		clienteController.setMiVentanaBuscar(miVentanaBuscar);
		clienteController.setMiVentanaListar(miVentanaListar);
		
		clienteController.setClienteServ(miclienteServ);
		
		
		//Video
		videoController.setMiVentanaPrincipal(miVentanaPrincipal);
		videoController.setMiVentanaRegistroVideo(miVentanaRegistroVideo);
		videoController.setMiVentanaBuscarVideo(miVentanaBuscarVideo);
		videoController.setMiVentanaListarVideo(miVentanaListarVideo);
		
		videoController.setVideoServ(mivideoServ);
			
		
		/*Se hace visible la ventana principal*/
		miVentanaPrincipal.setVisible(true);
	}

}
