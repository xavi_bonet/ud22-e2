package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import c4.ud22_patronMVC.controller.ClienteController;
import c4.ud22_patronMVC.model.dto.Cliente;
import c4.ud22_patronMVC.model.service.ClienteServ;

public class VentanaListar extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private ClienteController clienteController; 
	
	private JButton botonVolver, botonListarClientes;
	
	private JTable lista;
	JScrollPane panelLista;
	
	public VentanaListar() {
	
	botonVolver = new JButton();
	botonVolver.setBounds(352, 422, 120, 25);
	botonVolver.setText("Cancelar");
	
	botonListarClientes = new JButton();
	botonListarClientes.setBounds(10, 422, 120, 25);
	botonListarClientes.setText("Listar Clientes");
	
	lista = new JTable();
	//lista
	lista.setBounds(10, 11, 462, 400);

	panelLista = new JScrollPane(lista);
	panelLista.setBounds(10, 11, 462, 400);
	
	botonVolver.addActionListener(this);
	botonListarClientes.addActionListener(this);
	
	getContentPane().add(botonVolver);
	getContentPane().add(botonListarClientes);
	//getContentPane().add(lista);
	getContentPane().add(panelLista);
	
	
	setSize(500, 500);
	setTitle("Listar Clientes");
	setLocationRelativeTo(null);
	setResizable(false);
	getContentPane().setLayout(null);
	
	}
	
	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonVolver) {
			this.dispose();		
		}
		if (e.getSource() == botonListarClientes) {
			
			ArrayList<Cliente> listaClientes = clienteController.listarClientes();
			
			if (listaClientes != null) {
				muestraClientes(listaClientes);
			} else if (ClienteServ.consultaCliente == true) {
				JOptionPane.showMessageDialog(null, "No hay Clientes", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}
		
	}
	

	private void muestraClientes(ArrayList<Cliente> misClientes) {

		// Crear la tabla con los datos del arraylist de clientes
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("ID");
	    model.addColumn("Nombre");
	    model.addColumn("Apellido");
	    model.addColumn("Direccion");
	    model.addColumn("DNI");
	    model.addColumn("Fecha");
		
		Iterator <Cliente> iterator = misClientes.iterator();
		Cliente item;
		int contador = 0;
		while (iterator.hasNext()) {
			item = iterator.next();
	    	model.insertRow(contador, new Object[] { item.getIdCliente(), item.getNombreCliente(), item.getApellidoCliente(), item.getDireccionCliente(), item.getDniCliente(), item.getFechaCliente() });
	        contador++;
	    }
		
	    lista.setModel(model);
	    
	}
}
