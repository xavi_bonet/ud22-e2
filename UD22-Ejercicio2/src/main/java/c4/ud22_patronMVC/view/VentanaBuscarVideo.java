package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.model.dto.Video;
import c4.ud22_patronMVC.model.service.VideoServ;
import c4.ud22_patronMVC.controller.VideoController;

public class VentanaBuscarVideo extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private VideoController videoController; // objeto clienteController que permite la relacion entre esta clase y
													// la clase clienteController
	private JTextField textCodVideo, textTitle, textDirector, textCli_id;
	private JLabel codVideo, title, apellido, direccion;
	private JButton botonGuardar, botonCancelar, botonBuscar, botonModificar, botonEliminar;

	/**
	 * constructor de la clase donde se inicializan todos los componentes de la
	 * ventana de busqueda
	 */
	public VentanaBuscarVideo() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 212, 120, 25);
		botonGuardar.setText("Guardar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(150, 248, 120, 25);
		botonCancelar.setText("Cancelar");

		botonBuscar = new JButton();
		botonBuscar.setBounds(221, 11, 50, 25);
		botonBuscar.setText("Ok");

		botonEliminar = new JButton();
		botonEliminar.setBounds(10, 248, 120, 25);
		botonEliminar.setText("Eliminar");

		botonModificar = new JButton();
		botonModificar.setBounds(150, 212, 120, 25);
		botonModificar.setText("Modificar");

		codVideo = new JLabel();
		codVideo.setText("Cod. Video");
		codVideo.setBounds(20, 11, 80, 25);
		getContentPane().add(codVideo);

		title = new JLabel();
		title.setText("Titulo");
		title.setBounds(20, 51, 80, 25);
		getContentPane().add(title);

		apellido = new JLabel();
		apellido.setText("Director");
		apellido.setBounds(20, 91, 80, 25);
		getContentPane().add(apellido);
		
		direccion = new JLabel();
		direccion.setText("cli_id");
		direccion.setBounds(20, 127, 80, 25);
		getContentPane().add(direccion);

		textCodVideo = new JTextField();
		textCodVideo.setBounds(80, 11, 131, 25);
		getContentPane().add(textCodVideo);

		textTitle = new JTextField();
		textTitle.setBounds(80, 51, 190, 25);
		getContentPane().add(textTitle);

		textDirector = new JTextField();
		textDirector.setBounds(80, 91, 190, 25);
		getContentPane().add(textDirector);
		
		textCli_id = new JTextField();
		textCli_id.setText("");
		textCli_id.setBounds(80, 127, 190, 25);
		getContentPane().add(textCli_id);

		botonModificar.addActionListener(this);
		botonEliminar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);

		getContentPane().add(botonCancelar);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonModificar);
		getContentPane().add(botonEliminar);
		getContentPane().add(botonGuardar);
		
		limpiar();

		setSize(300, 335);
		setTitle("Administrar Videos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

		

	}

	public void setCoordinador(VideoController videoController) {
		this.videoController = videoController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Video miVideo = new Video();
				miVideo.setIdVideo(Integer.parseInt(textCodVideo.getText()));
				miVideo.setTitle(textTitle.getText());
				miVideo.setDirector(textDirector.getText());
				miVideo.setCli_id(Integer.parseInt(textCli_id.getText()));

				videoController.modificarVideo(miVideo);

				if (VideoServ.modificaVideo == true) {
					limpiar();
				}
			} catch (Exception e2) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
			}

		}

		if (e.getSource() == botonBuscar) {
			Video miVideo = videoController.buscarVideo(textCodVideo.getText());
			if (miVideo != null) {
				muestraVideo(miVideo);
			} else if (VideoServ.consultaVideo == true) {
				JOptionPane.showMessageDialog(null, "El video no Existe", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}

		if (e.getSource() == botonModificar) {
			habilita(false, true, true, false, false, true, false, false);

		}

		if (e.getSource() == botonEliminar) {
			if (!textCodVideo.getText().equals("")) {
				int respuesta = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el video?",
						"Confirmación", JOptionPane.YES_NO_OPTION);
				if (respuesta == JOptionPane.YES_NO_OPTION) {
					videoController.eliminarVideo(textCodVideo.getText());
					limpiar();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Ingrese un numero de Video", "Información",
						JOptionPane.WARNING_MESSAGE);
			}

		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}

	}

	/**
	 * permite cargar los datos de la persona consultada
	 * 
	 * @param miVideo
	 */
	private void muestraVideo(Video miVideo) {
		textTitle.setText(miVideo.getTitle());
		textDirector.setText(miVideo.getDirector());
		textCli_id.setText(miVideo.getCli_id().toString());
		habilita(true, false, false, false, true, false, true, true);
	}

	/**
	 * Permite limpiar los componentes
	 */
	public void limpiar() {
		textCodVideo.setText("");
		textTitle.setText("");
		textDirector.setText("");
		textCli_id.setText("");
		habilita(true, false, false, false, true, false, true, true);
	}

	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * 
	 * @param codigo
	 * @param title
	 * @param director
	 * @param cli_id
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */
	public void habilita(boolean codigo, boolean title, boolean director, boolean cli_id, boolean bBuscar, boolean bGuardar, boolean bModificar, boolean bEliminar) {
		textCodVideo.setEditable(codigo);
		textTitle.setEditable(title);
		textDirector.setEditable(director);
		textCli_id.setEditable(cli_id);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
		botonModificar.setEnabled(bModificar);
		botonEliminar.setEnabled(bEliminar);
	}
}
