package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.ClienteController;
import c4.ud22_patronMVC.model.dto.Cliente;

public class VentanaRegistro extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private ClienteController clienteController; 
	
	private JButton botonGuardar, botonCancelar;
	private JLabel nombre, apellido, dni, direccion;
	private JTextField textNombre, textApellido, textDni, textDireccion;

	/**
	 * constructor de la clase donde se inicializan todos los componentes de la
	 * ventana de registro
	 */
	public VentanaRegistro() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 193, 120, 25);
		botonGuardar.setText("Registrar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(140, 193, 120, 25);
		botonCancelar.setText("Cancelar");

		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);

		nombre = new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(10, 23, 80, 25);
		getContentPane().add(nombre);

		apellido = new JLabel();
		apellido.setText("Apellido");
		apellido.setBounds(10, 63, 80, 25);
		getContentPane().add(apellido);

		dni = new JLabel();
		dni.setText("Dni");
		dni.setBounds(10, 135, 80, 25);
		getContentPane().add(dni);

		textNombre = new JTextField();
		textNombre.setText("");
		textNombre.setBounds(70, 23, 190, 25);
		getContentPane().add(textNombre);

		textApellido = new JTextField();
		textApellido.setText("");
		textApellido.setBounds(70, 63, 190, 25);
		getContentPane().add(textApellido);

		textDni = new JTextField();
		textDni.setText("");
		textDni.setBounds(70, 135, 190, 25);
		getContentPane().add(textDni);

		direccion = new JLabel();
		direccion.setText("Direccion");
		direccion.setBounds(10, 99, 80, 25);
		getContentPane().add(direccion);

		textDireccion = new JTextField();
		textDireccion.setText("");
		textDireccion.setBounds(70, 99, 190, 25);
		getContentPane().add(textDireccion);
		
		limpiar();
		setSize(300, 335);
		setTitle("Registro de Clientes");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

	}

	private void limpiar() {
	}

	public void setCoordinador(ClienteController clienteController) {
		this.clienteController = clienteController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Cliente miCliente = new Cliente();
				miCliente.setNombreCliente(textNombre.getText());
				miCliente.setApellidoCliente(textApellido.getText());
				miCliente.setDireccionCliente(textDireccion.getText());
				miCliente.setDniCliente(Integer.parseInt(textDni.getText()));

				clienteController.registrarCliente(miCliente);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}
	}

}
