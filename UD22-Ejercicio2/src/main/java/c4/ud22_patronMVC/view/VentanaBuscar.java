package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.model.dto.Cliente;
import c4.ud22_patronMVC.model.service.ClienteServ;
import c4.ud22_patronMVC.controller.ClienteController;

public class VentanaBuscar extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private ClienteController clienteController; // objeto clienteController que permite la relacion entre esta clase y
													// la clase clienteController
	private JTextField textCod, textNombre, textDni, textApellido, textDireccion;
	private JLabel cod, nombre, dni, apellido, direccion;
	private JButton botonGuardar, botonCancelar, botonBuscar, botonModificar, botonEliminar;

	/**
	 * constructor de la clase donde se inicializan todos los componentes de la
	 * ventana de busqueda
	 */
	public VentanaBuscar() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 212, 120, 25);
		botonGuardar.setText("Guardar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(150, 248, 120, 25);
		botonCancelar.setText("Cancelar");

		botonBuscar = new JButton();
		botonBuscar.setBounds(221, 11, 50, 25);
		botonBuscar.setText("Ok");

		botonEliminar = new JButton();
		botonEliminar.setBounds(10, 248, 120, 25);
		botonEliminar.setText("Eliminar");

		botonModificar = new JButton();
		botonModificar.setBounds(150, 212, 120, 25);
		botonModificar.setText("Modificar");

		cod = new JLabel();
		cod.setText("Codigo");
		cod.setBounds(20, 11, 80, 25);
		getContentPane().add(cod);

		nombre = new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(20, 51, 80, 25);
		getContentPane().add(nombre);

		apellido = new JLabel();
		apellido.setText("Apellido");
		apellido.setBounds(20, 91, 80, 25);
		getContentPane().add(apellido);

		dni = new JLabel();
		dni.setText("Dni");
		dni.setBounds(20, 163, 80, 25);
		getContentPane().add(dni);
		
		direccion = new JLabel();
		direccion.setText("Direccion");
		direccion.setBounds(20, 127, 80, 25);
		getContentPane().add(direccion);

		textCod = new JTextField();
		textCod.setBounds(80, 11, 131, 25);
		getContentPane().add(textCod);

		textNombre = new JTextField();
		textNombre.setBounds(80, 51, 190, 25);
		getContentPane().add(textNombre);

		textApellido = new JTextField();
		textApellido.setBounds(80, 91, 190, 25);
		getContentPane().add(textApellido);

		textDni = new JTextField();
		textDni.setBounds(80, 163, 190, 25);
		getContentPane().add(textDni);
		
		textDireccion = new JTextField();
		textDireccion.setText("");
		textDireccion.setBounds(80, 127, 190, 25);
		getContentPane().add(textDireccion);

		botonModificar.addActionListener(this);
		botonEliminar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);

		getContentPane().add(botonCancelar);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonModificar);
		getContentPane().add(botonEliminar);
		getContentPane().add(botonGuardar);
		
		limpiar();

		setSize(300, 335);
		setTitle("Administrar Clientes");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

		

	}

	public void setCoordinador(ClienteController clienteController) {
		this.clienteController = clienteController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Cliente miCliente = new Cliente();
				miCliente.setIdCliente(Integer.parseInt(textCod.getText()));
				miCliente.setNombreCliente(textNombre.getText());
				miCliente.setApellidoCliente(textApellido.getText());
				miCliente.setDireccionCliente(textDireccion.getText());
				miCliente.setDniCliente(Integer.parseInt(textDni.getText()));

				clienteController.modificarCliente(miCliente);

				if (ClienteServ.modificaCliente == true) {
					habilita(true, false, false, false, false, true, false, true, true);
				}
			} catch (Exception e2) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
			}

		}

		if (e.getSource() == botonBuscar) {
			Cliente miCliente = clienteController.buscarCliente(textCod.getText());
			if (miCliente != null) {
				muestraCliente(miCliente);
			} else if (ClienteServ.consultaCliente == true) {
				JOptionPane.showMessageDialog(null, "El cliente no Existe", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}

		if (e.getSource() == botonModificar) {
			habilita(false, true, true, true, true, false, true, false, false);

		}

		if (e.getSource() == botonEliminar) {
			if (!textCod.getText().equals("")) {
				int respuesta = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el cliente?",
						"Confirmación", JOptionPane.YES_NO_OPTION);
				if (respuesta == JOptionPane.YES_NO_OPTION) {
					clienteController.eliminarCliente(textCod.getText());
					limpiar();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Ingrese un numero de Documento", "Información",
						JOptionPane.WARNING_MESSAGE);
			}

		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}

	}

	/**
	 * permite cargar los datos de la persona consultada
	 * 
	 * @param miPersona
	 */
	private void muestraCliente(Cliente miCliente) {
		textNombre.setText(miCliente.getNombreCliente());
		textApellido.setText(miCliente.getApellidoCliente());
		textDireccion.setText(miCliente.getDireccionCliente());
		textDni.setText(miCliente.getDniCliente() + "");
		habilita(true, false, false, false, false, true, false, true, true);
	}

	/**
	 * Permite limpiar los componentes
	 */
	public void limpiar() {
		textCod.setText("");
		textNombre.setText("");
		textApellido.setText("");
		textDireccion.setText("");
		textDni.setText("");
		habilita(true, false, false, false, false, true, false, true, true);
	}

	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * 
	 * @param codigo
	 * @param nombre
	 * @param apellido
	 * @param direccion
	 * @param dni
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */
	public void habilita(boolean codigo, boolean nombre, boolean apellido, boolean direccion, boolean dni,
			boolean bBuscar, boolean bGuardar, boolean bModificar, boolean bEliminar) {
		textCod.setEditable(codigo);
		textNombre.setEditable(nombre);
		textApellido.setEditable(apellido);
		textDireccion.setEditable(direccion);
		textDni.setEditable(dni);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
		botonModificar.setEnabled(bModificar);
		botonEliminar.setEnabled(bEliminar);
	}
}
