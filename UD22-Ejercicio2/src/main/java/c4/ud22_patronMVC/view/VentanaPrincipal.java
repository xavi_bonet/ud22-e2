package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import c4.ud22_patronMVC.controller.ClienteController;
import c4.ud22_patronMVC.controller.VideoController;

import javax.swing.JSplitPane;

public class VentanaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private ClienteController clienteController; //objeto ClienteController que permite la relacion entre esta clase y la clase ClienteController
	private VideoController videoController; //objeto VideoController que permite la relacion entre esta clase y la clase VideoController
	private JLabel labelTitulo;
	private JButton botonRegistrar,botonBuscar, btnBuscarVideo, btnRegistrarVideo, botonListar, botonListarVideo;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana principal
	 */
	public VentanaPrincipal() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		labelTitulo = new JLabel();
		labelTitulo.setText("Ud22 - Patron MVC");
		labelTitulo.setBounds(33, 11, 164, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 15));

		botonRegistrar = new JButton();
		botonRegistrar.setBounds(33, 62, 163, 25);
		botonRegistrar.setText("Registrar Cliente");
		
		botonBuscar = new JButton();
		botonBuscar.setBounds(33, 86, 163, 25);
		botonBuscar.setText("Buscar Cliente");
		
		botonListar = new JButton();
		botonListar.setBounds(33, 111, 163, 25);
		botonListar.setText("Listar Clientes");
		
		btnRegistrarVideo = new JButton();
		btnRegistrarVideo.setText("Registrar Video");
		btnRegistrarVideo.setBounds(33, 147, 163, 25);
		
		btnBuscarVideo = new JButton();
		btnBuscarVideo.setText("Buscar Video");
		btnBuscarVideo.setBounds(33, 172, 163, 25);
		
		botonListarVideo = new JButton();
		botonListarVideo.setBounds(33, 196, 163, 25);
		botonListarVideo.setText("Listar Videos");
		
		btnRegistrarVideo.addActionListener(this);
		btnBuscarVideo.addActionListener(this);
		botonRegistrar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonListar.addActionListener(this);
		botonListarVideo.addActionListener(this);
		
		getContentPane().add(botonListar);
		getContentPane().add(botonListarVideo);
		getContentPane().add(btnBuscarVideo);
		getContentPane().add(btnRegistrarVideo);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonRegistrar);
		getContentPane().add(labelTitulo);
	
		setSize(238, 320);
		setTitle("Clientes / Videos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		
	}


	public void setCoordinador(ClienteController clienteController, VideoController videoController) {
		this.clienteController=clienteController;
		this.videoController=videoController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonRegistrar) {
			clienteController.mostrarVentanaRegistro();			
		}
		if (e.getSource()==botonBuscar) {
			clienteController.mostrarVentanaConsulta();			
		}
		if (e.getSource()==botonListar) {
			clienteController.mostrarVentanaListar();
		}
		if (e.getSource()==btnRegistrarVideo) {
			videoController.mostrarVentanaRegistroVideo();			
		}
		if (e.getSource()==btnBuscarVideo) {
			videoController.mostrarVentanaConsultaVideo();			
		}
		if (e.getSource()==botonListarVideo) {
			videoController.mostrarVentanaListarVideo();
		}
		
	}
}
