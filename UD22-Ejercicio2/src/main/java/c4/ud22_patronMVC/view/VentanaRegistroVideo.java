package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.ClienteController;
import c4.ud22_patronMVC.controller.VideoController;
import c4.ud22_patronMVC.model.dto.Cliente;
import c4.ud22_patronMVC.model.dto.Video;
import c4.ud22_patronMVC.model.service.ClienteServ;

public class VentanaRegistroVideo extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private ClienteController clienteController;
	private VideoController videoController;

	private JButton botonGuardar, botonCancelar, botonBuscar, botonLimpiar;
	private JLabel titulo, director, dni, nombreCliente, lblCodigoCliente;
	private JTextField textTitulo, textDirector, textDni, textNombre, textCod;

	/**
	 * constructor de la clase donde se inicializan todos los componentes de la
	 * ventana de registro
	 */
	public VentanaRegistroVideo() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 202, 120, 25);
		botonGuardar.setText("Registrar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(140, 202, 120, 25);
		botonCancelar.setText("Cancelar");
		
		botonBuscar = new JButton();
		botonBuscar.setText("Ok");
		botonBuscar.setBounds(210, 16, 50, 25);
		
		botonLimpiar = new JButton();
		botonLimpiar.setText("Limpiar");
		botonLimpiar.setBounds(10, 238, 250, 25);
		
		

		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonLimpiar.addActionListener(this);
		getContentPane().add(botonLimpiar);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);

		titulo = new JLabel();
		titulo.setText("Titulo");
		titulo.setBounds(10, 52, 80, 25);
		getContentPane().add(titulo);

		director = new JLabel();
		director.setText("Director");
		director.setBounds(10, 92, 80, 25);
		getContentPane().add(director);

		dni = new JLabel();
		dni.setText("Dni");
		dni.setBounds(10, 164, 80, 25);
		getContentPane().add(dni);

		textTitulo = new JTextField();
		textTitulo.setEditable(false);
		textTitulo.setText("");
		textTitulo.setBounds(70, 52, 190, 25);
		getContentPane().add(textTitulo);

		textDirector = new JTextField();
		textDirector.setEditable(false);
		textDirector.setText("");
		textDirector.setBounds(70, 92, 190, 25);
		getContentPane().add(textDirector);

		textDni = new JTextField();
		textDni.setEditable(false);
		textDni.setText("");
		textDni.setBounds(70, 164, 190, 25);
		getContentPane().add(textDni);

		nombreCliente = new JLabel();
		nombreCliente.setText("Cliente");
		nombreCliente.setBounds(10, 128, 80, 25);
		getContentPane().add(nombreCliente);

		textNombre = new JTextField();
		textNombre.setEditable(false);
		textNombre.setText("");
		textNombre.setBounds(70, 128, 190, 25);
		getContentPane().add(textNombre);

		lblCodigoCliente = new JLabel();
		lblCodigoCliente.setText("Cod. Cliente");
		lblCodigoCliente.setBounds(9, 16, 80, 25);
		getContentPane().add(lblCodigoCliente);

		textCod = new JTextField();
		textCod.setText("");
		textCod.setEditable(true);
		textCod.setBounds(69, 16, 131, 25);
		getContentPane().add(textCod);
		

		limpiar();

		setSize(300, 335);
		setTitle("Registro de Videos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		
		

	}

	private void limpiar() {
		textCod.setText("");
		textTitulo.setText("");
		textDirector.setText("");
		textDni.setText("");
		textNombre.setText("");
		habilita(true, false, false, true, false);
	}

	public void setCoordinador(ClienteController clienteController, VideoController videoController) {
		this.clienteController = clienteController;
		this.videoController = videoController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Video miVideo = new Video();
				miVideo.setCli_id(Integer.parseInt(textCod.getText()));
				miVideo.setTitle(textTitulo.getText());
				miVideo.setDirector(textDirector.getText());
				videoController.registrarVideo(miVideo);
				limpiar();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
				limpiar();
			}
		}
		if (e.getSource() == botonLimpiar) {
			limpiar();
		}
		if (e.getSource() == botonCancelar) {
			limpiar();
			this.dispose();
		}
		if (e.getSource() == botonBuscar) {
			Cliente miCliente = clienteController.buscarCliente(textCod.getText());
			if (miCliente != null) {
				muestraCliente(miCliente);
			} else if (ClienteServ.consultaCliente == true) {
				JOptionPane.showMessageDialog(null, "El cliente no Existe", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	private void muestraCliente(Cliente miCliente) {
		textNombre.setText(miCliente.getNombreCliente());
		textDni.setText(miCliente.getDniCliente() + "");
		habilita(false, true, true, false, true);
	}
	
	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * 
	 * @param tCod
	 * @param tTitulo
	 * @param tDirector
	 * @param bBuscar
	 * @param bGuardar
	 */
	public void habilita(boolean tCod, boolean tTitulo, boolean tDirector, boolean bBuscar, boolean bGuardar) {
		textCod.setEditable(tCod);
		textTitulo.setEditable(tTitulo);
		textDirector.setEditable(tDirector);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
	}
}
