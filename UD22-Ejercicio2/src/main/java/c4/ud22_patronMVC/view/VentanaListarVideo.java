package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import c4.ud22_patronMVC.controller.VideoController;
import c4.ud22_patronMVC.model.dto.Video;
import c4.ud22_patronMVC.model.service.ClienteServ;

public class VentanaListarVideo extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private VideoController videoController; 
	
	private JButton botonVolver, botonListarClientes;
	
	private JTable lista;
	JScrollPane panelLista;
	
	public VentanaListarVideo() {
	
	botonVolver = new JButton();
	botonVolver.setBounds(352, 422, 120, 25);
	botonVolver.setText("Cancelar");
	
	botonListarClientes = new JButton();
	botonListarClientes.setBounds(10, 422, 120, 25);
	botonListarClientes.setText("Listar Videos");
	
	lista = new JTable();
	//lista
	lista.setBounds(10, 11, 462, 400);

	panelLista = new JScrollPane(lista);
	panelLista.setBounds(10, 11, 462, 400);
	
	botonVolver.addActionListener(this);
	botonListarClientes.addActionListener(this);
	
	getContentPane().add(botonVolver);
	getContentPane().add(botonListarClientes);
	//getContentPane().add(lista);
	getContentPane().add(panelLista);
	
	
	setSize(500, 500);
	setTitle("Listar Videos");
	setLocationRelativeTo(null);
	setResizable(false);
	getContentPane().setLayout(null);
	
	}
	
	public void setCoordinador(VideoController videoController) {
		this.videoController=videoController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonVolver) {
			this.dispose();		
		}
		if (e.getSource() == botonListarClientes) {
			
			ArrayList<Video> listaVideos = videoController.listarVideos();
			
			if (listaVideos != null) {
				muestraVideos(listaVideos);
			} else if (ClienteServ.consultaCliente == true) {
				JOptionPane.showMessageDialog(null, "No hay Videos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}
		
	}
	

	private void muestraVideos(ArrayList<Video> misVideos) {

		// Crear la tabla con los datos del arraylist de clientes
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("id");
	    model.addColumn("titulo");
	    model.addColumn("director");
	    model.addColumn("cli_id");
		
		Iterator <Video> iterator = misVideos.iterator();
		Video item;
		int contador = 0;
		while (iterator.hasNext()) {
			item = iterator.next();
	    	model.insertRow(contador, new Object[] { item.getIdVideo(), item.getTitle(), item.getDirector(), item.getCli_id() });
	        contador++;
	    }
		
	    lista.setModel(model);
	    
	}
}
