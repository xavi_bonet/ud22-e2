package c4.ud22_patronMVC.model.conexion;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que permite conectar con la base de datos
 *
 */
public class Conexion {
	private final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private String hostname;
	private String username;
	private String password;
	private String port;
	private String bd;
	private Connection conn = null;

	// Constructor de DbConnection
	public Conexion() {
		this.hostname = "192.168.1.200";
		this.username = "remote";
		this.password = "&BAf7N6&*?TW";
		this.port = "3306";
		this.bd = "UD22-E2";
		this.conn = Connection_DB();
	}

	private Connection Connection_DB() {
		Connection connection = null;
		try {
			// Obtenemos la url a la que conectarnos
			String url = "jdbc:mysql://" + hostname + ":" + port + "/" + bd + "?useTimezone=true&serverTimezone=UTC";

			// Obtenemos el driver de para mysql
			Class.forName(DRIVER);

			// Obtenemos la conexion
			connection = DriverManager.getConnection(url, username, password);

			if (conn != null) {
				System.out.print("Conexión a base de datos " + bd + "_SUCCESS at");
				fecha();
			}
		} catch (SQLException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}

		return connection;
	}

	// Permite retornar la conexion
	public Connection getConnection() {
		return conn;
	}
	
	// Permite desconectarse de la BD
	public void desconectar() {
  		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// Permite ejecutar una query y retorna true o false segun si se ejecuto o no
	public boolean executeQuery(String query) {
        try {
            Statement st= conn.createStatement();
            st.executeUpdate(query);
            st.close();
            System.out.println("Query ejecutada con exito!");
            return true;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
	
	//obtiene valores
    public ResultSet getValues(String query) {
        try {
            Statement st = conn.createStatement();
            ResultSet ResultSet = st.executeQuery(query);
            return ResultSet;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

	// METODO QUE MUESTRA FECHA
	public static void fecha() {
		Date date = new Date();
		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		System.out.println(" - " + hourdateFormat.format(date));
	}

}