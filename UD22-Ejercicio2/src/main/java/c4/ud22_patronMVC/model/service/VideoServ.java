/*
 * Esta clase permite realizar las operaciones asociadas a la lógica de negocio como tal, desde ella realizamos las validaciones 
 * y llamadas a las operaciones CRUD del sistema.
 * 
 * En caso de que se requieran procesos adicionales asociados a la lógica de negocio, aquí será donde se creen los métodos para 
 * dichos procesos, por ejemplo el método validarRegistro determina si los datos son correctos y permite registrar la persona en
 * el Dao.
 */

package c4.ud22_patronMVC.model.service;

import javax.swing.JOptionPane;

import c4.ud22_patronMVC.model.dao.VideoDao;
import c4.ud22_patronMVC.model.dto.Video;
import c4.ud22_patronMVC.controller.VideoController;

public class VideoServ {
	
	private VideoController videoController; 
	public static boolean consultaVideo=false;
	public static boolean modificaVideo=false;
	
	//Metodo de vinculación con el controller principal
	public void setpersonaController(VideoController videoController) {
		this.setController(videoController);		
	}

	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Video miVideo) {
		VideoDao miClienteDao;
		if (miVideo.getTitle().toString().length() <= 200  ) {
			miClienteDao = new VideoDao();
			miClienteDao.registrarVideo(miVideo);						
		}else {
			JOptionPane.showMessageDialog(null,"El titulo no puede tener mas de 200 letras","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
		
	}
	
	//Metodo que valida los datos de consulta antes de pasar estos al DAO
	public Video validarConsulta(String codigoVideo) {
		VideoDao miVideoDao;
		
		try {
			int codigo=Integer.parseInt(codigoVideo);	
			if (codigo > 0) {
				miVideoDao = new VideoDao();
				consultaVideo=true;
				return miVideoDao.buscarVideo(codigo);						
			}else{
				JOptionPane.showMessageDialog(null,"El id del video ha de ser mayor que 0","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaVideo=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		}
					
		return null;
	}

	//Metodo que valida los datos de Modificación antes de pasar estos al DAO
	public void validarModificacion(Video miVideo) {
		VideoDao miVideoDao;
		if (miVideo.getTitle().length()>3) {
			miVideoDao = new VideoDao();
			miVideoDao.modificarVideo(miVideo);	
			modificaVideo=true;
		}else{
			JOptionPane.showMessageDialog(null,"El titulo ha de tener almenos 3 letras","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaVideo=false;
		}
	}

	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String codigo) {
		VideoDao miVideoDao=new VideoDao();
		miVideoDao.eliminarVideo(codigo);
	}

	
	
	public VideoController getVideoController() {
		return videoController;
	}

	public void setController(VideoController videoController) {
		this.videoController = videoController;
	}



}
