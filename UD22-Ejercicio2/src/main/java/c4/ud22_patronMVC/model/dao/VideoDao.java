package c4.ud22_patronMVC.model.dao;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import c4.ud22_patronMVC.model.conexion.Conexion;
import c4.ud22_patronMVC.model.dto.Cliente;
import c4.ud22_patronMVC.model.dto.Video;

/**
 * Clase que permite el acceso a la base de datos CRUD
 *
 */
public class VideoDao {

	public void registrarVideo(Video miVideo) {
		Conexion conex = new Conexion();
		try {
			// Query que vamos a ejecutar
			String sql = "INSERT INTO videos(title, director, cli_id) VALUES ('"
					+ miVideo.getTitle() + "', '" 
					+ miVideo.getDirector() + "', '"
					+ miVideo.getCli_id() + "');";

			// Ejecutamos la query, si devuelve false lanzamos un error!
			if (!conex.executeQuery(sql))
					throw new Exception();

			// Mensajes de informacion
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente", "Información",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);

			// Cerramos la conexión
			conex.desconectar();

		} catch (Exception e) {
			// Cerramos la conexión
			conex.desconectar();
			
			// Mensajes de informacion
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}

	public Video buscarVideo(int codigo) {
		Conexion conex = new Conexion();
		Video video = new Video();
		boolean existe = false;
		try {
			// Query que vamos a ejecutar
			String sql = "SELECT * FROM videos where id = "+codigo+";";
			
			// Ejecutamos la query
			ResultSet res = conex.getValues(sql);
			
			// Si esta vacio lanzamos error
			if (res == null)
				throw new Exception();

			while (res.next()) {
				existe = true;
				video.setIdVideo(Integer.parseInt(res.getString("id")));
				video.setTitle(res.getString("title"));
				video.setDirector(res.getString("director"));
				video.setCli_id(Integer.parseInt(res.getString("cli_id")));
			}
			
			// Si esta vacio lanzamos error
			if (!existe)
				throw new Exception();
			
			// Cerramos la conexión
			res.close();
			conex.desconectar();
			
			// Mensajes de informacion
			System.out.println(sql);
			
			// Devolvemos el cliente
			return video;
		} catch (Exception e) {
			// Mensajes de informacion
			System.out.println("Error, no se encontro");
			
			// Devolvemos null
			return null;
		}
			
	}

	public void modificarVideo(Video miVideo) {

		Conexion conex = new Conexion();
		try {
			// Valores necesarios para la query
			int id = miVideo.getIdVideo();
			String title = miVideo.getTitle();
			String director = miVideo.getDirector();
			int cli_id = miVideo.getCli_id();
			
			//Fecha del sistema para hacer el update
		    long millis=System.currentTimeMillis();   
		    Date dt = new Date(millis);
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    System.out.println(sdf.format(dt)); 

			// Query que vamos a ejecutar
			String sql = "UPDATE videos SET title='"+title+"', director='"+director+"', cli_id='"+cli_id+"' WHERE id='"+id+"';";

			// Ejecutamos la query, si devuelve false lanzamos un error!
			if (!conex.executeQuery(sql))
				throw new Exception();

			// Mensajes de informacion
			JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ", "Confirmación",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);

			// Cerramos la conexión
			conex.desconectar();

		} catch (Exception e) {
			// Cerramos la conexión
			conex.desconectar();
				
			// Mensajes de informacion
			System.out.println(e);
			JOptionPane.showMessageDialog(null, "Error al Modificar", "Error", JOptionPane.ERROR_MESSAGE);

		}
	}

	public void eliminarVideo(String codigo) {
		Conexion conex = new Conexion();
		try {
			// Query que vamos a ejecutar
			String sql = "DELETE FROM videos WHERE id=" + codigo + ";";

			// Ejecutamos la query, si devuelve false lanzamos un error!
			if (!conex.executeQuery(sql))
				throw new Exception();

			// Mensajes de informacion
			JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente", "Información",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);

			// Cerramos la conexión
			conex.desconectar();

		} catch (Exception e) {
			// Cerramos la conexión
			conex.desconectar();
				
			// Mensajes de informacion
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

	public ArrayList<Video> listarVideos() {
		// Abrir la conexion a la base de datos
				Conexion conex = new Conexion();
				boolean existe = false;
				try {
					ArrayList<Video> listaVideos = new ArrayList<Video>();
					

					// Ejecutar Query que obtiene el cliente, guardar los datos en el resultSet i
					// guardarlos despues en el objeto cliente
					ResultSet res = conex.getValues("SELECT * FROM videos;");
					
					// Guardar los datos en el objeto cliente
					while (res.next()) {
						existe = true;
						Video video = new Video();
						video.setIdVideo(Integer.parseInt(res.getString("id")));
						video.setTitle(res.getString("title"));
						video.setDirector(res.getString("director"));
						video.setCli_id(Integer.parseInt(res.getString("cli_id")));
						listaVideos.add(video);
					}
					// Cerrar conexion
					conex.desconectar();
					
					// Si el cliente no existe, lanzar un error
					if (!existe)
						throw new Exception();
					
					return listaVideos;
				} catch (Exception e) {
					// Cerrar conexion
					conex.desconectar();
					System.out.println("Error: "+e);
					return null;
				}
	}

}
