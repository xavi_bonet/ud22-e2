package c4.ud22_patronMVC.model.dao;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import c4.ud22_patronMVC.model.conexion.Conexion;
import c4.ud22_patronMVC.model.dto.Cliente;

/**
 * Clase que permite el acceso a la base de datos CRUD
 *
 */
public class ClienteDao {

	public void registrarCliente(Cliente miCliente) {
		Conexion conex = new Conexion();
		try {
			//Fecha del sistema para hacer el insert
		    long millis=System.currentTimeMillis();   
		    Date dt = new Date(millis);
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    System.out.println(sdf.format(dt)); 
			
			// Query que vamos a ejecutar
			String sql = "INSERT INTO cliente(nombre, apellido, direccion, dni, fecha) VALUES ('"
					+ miCliente.getNombreCliente() + "', '" + miCliente.getApellidoCliente() + "', '"
					+ miCliente.getDireccionCliente() + "', '" + miCliente.getDniCliente() + "', '"
					+ sdf.format(dt) + "');";

			// Ejecutamos la query, si devuelve false lanzamos un error!
			if (!conex.executeQuery(sql))
					throw new Exception();

			// Mensajes de informacion
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente", "Información",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);

			// Cerramos la conexión
			conex.desconectar();

		} catch (Exception e) {
			// Cerramos la conexión
			conex.desconectar();
			
			// Mensajes de informacion
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}

	public Cliente buscarCliente(int codigo) {
		Conexion conex = new Conexion();
		Cliente cliente = new Cliente();
		boolean existe = false;
		try {
			// Query que vamos a ejecutar
			String sql = "SELECT * FROM cliente where id = "+codigo+";";
			
			// Ejecutamos la query
			ResultSet res = conex.getValues(sql);
			
			while (res.next()) {
				existe = true;
				cliente.setIdCliente(Integer.parseInt(res.getString("id")));
				cliente.setNombreCliente(res.getString("nombre"));
				cliente.setApellidoCliente(res.getString("apellido"));
				cliente.setDireccionCliente(res.getString("direccion"));
				cliente.setDniCliente(Integer.parseInt(res.getString("dni")));
				cliente.setFechaCliente(res.getString("fecha"));
			}
			
			// Si esta vacio lanzamos error
			if (!existe)
				throw new Exception();
			
			// Cerramos la conexión
			res.close();
			conex.desconectar();
			
			// Mensajes de informacion
			System.out.println(sql);
			
			// Devolvemos el cliente
			return cliente;
		} catch (Exception e) {
			// Mensajes de informacion
			System.out.println("Error, no se encontro");
			
			// Devolvemos null
			return null;
		}
			
	}

	public void modificarCliente(Cliente miCliente) {

		Conexion conex = new Conexion();
		try {
			// Valores necesarios para la query
			int id = miCliente.getIdCliente();
			String nombre = miCliente.getNombreCliente();
			String apellido = miCliente.getApellidoCliente();
			String direccion = miCliente.getDireccionCliente();
			int dni = miCliente.getDniCliente();
			
			//Fecha del sistema para hacer el update
		    long millis=System.currentTimeMillis();   
		    Date dt = new Date(millis);
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    System.out.println(sdf.format(dt)); 

			// Query que vamos a ejecutar
			String sql = "UPDATE cliente SET nombre='"+nombre+"', apellido='"+apellido+"', direccion='"+direccion+"', dni='"+dni+"', fecha='"+sdf.format(dt)+"' WHERE id='"+id+"';";

			// Ejecutamos la query, si devuelve false lanzamos un error!
			if (!conex.executeQuery(sql))
				throw new Exception();

			// Mensajes de informacion
			JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ", "Confirmación",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);

			// Cerramos la conexión
			conex.desconectar();

		} catch (Exception e) {
			// Cerramos la conexión
			conex.desconectar();
				
			// Mensajes de informacion
			System.out.println(e);
			JOptionPane.showMessageDialog(null, "Error al Modificar", "Error", JOptionPane.ERROR_MESSAGE);

		}
	}

	public void eliminarCliente(String codigo) {
		Conexion conex = new Conexion();
		try {
			// Query que vamos a ejecutar
			String sql = "DELETE FROM cliente WHERE id=" + codigo + ";";

			// Ejecutamos la query, si devuelve false lanzamos un error!
			if (!conex.executeQuery(sql))
				throw new Exception();

			// Mensajes de informacion
			JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente", "Información",
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);

			// Cerramos la conexión
			conex.desconectar();

		} catch (Exception e) {
			// Cerramos la conexión
			conex.desconectar();
				
			// Mensajes de informacion
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

	public ArrayList<Cliente> listarClientes() {
		// Abrir la conexion a la base de datos
		Conexion conex = new Conexion();
		boolean existe = false;
		try {
			ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
			

			// Ejecutar Query que obtiene el cliente, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cliente
			ResultSet res = conex.getValues("SELECT * FROM cliente;");
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				Cliente cliente = new Cliente();
				cliente.setIdCliente(Integer.parseInt(res.getString("id")));
				cliente.setNombreCliente(res.getString("nombre"));
				cliente.setApellidoCliente(res.getString("apellido"));
				cliente.setDireccionCliente(res.getString("direccion"));
				cliente.setDniCliente(Integer.parseInt(res.getString("dni")));
				cliente.setFechaCliente(res.getString("fecha"));
				listaClientes.add(cliente);
			}
			// Cerrar conexion
			conex.desconectar();
			
			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return listaClientes;
		} catch (Exception e) {
			// Cerrar conexion
			conex.desconectar();
			System.out.println("Error: "+e);
			return null;
		}
	}
	

}
