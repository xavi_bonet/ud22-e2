
/*
 * Esta parte del patrón es la que define la lógica de administración del sistema, 
 * establece la conexión entre la vista y el modelo.
 */

package c4.ud22_patronMVC.controller;

import java.util.ArrayList;

import c4.ud22_patronMVC.model.dao.VideoDao;
import c4.ud22_patronMVC.model.dto.Video;
import c4.ud22_patronMVC.model.service.VideoServ;
import c4.ud22_patronMVC.view.VentanaBuscarVideo;
import c4.ud22_patronMVC.view.VentanaListarVideo;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.VentanaRegistroVideo;


public class VideoController {
	
	private VideoServ videoServ;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistroVideo miVentanaRegistroVideo;
	private VentanaBuscarVideo miVentanaBuscarVideo;
	private VentanaListarVideo miVentanaListarVideo;
	
	//Metodos getter Setters de vistas
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public VentanaRegistroVideo getMiVentanaRegistroVideo() {
		return miVentanaRegistroVideo;
	}
	public void setMiVentanaRegistroVideo(VentanaRegistroVideo miVentanaRegistro) {
		this.miVentanaRegistroVideo = miVentanaRegistro;
	}
	public VentanaBuscarVideo getMiVentanaBuscarVideo() {
		return miVentanaBuscarVideo;
	}
	public void setMiVentanaBuscarVideo(VentanaBuscarVideo miVentanaBuscar) {
		this.miVentanaBuscarVideo = miVentanaBuscar;
	}
	public VideoServ getVideoServ() {
		return videoServ;
	}
	public void setVideoServ(VideoServ clienteServ) {
		this.videoServ = clienteServ;
	}
	
	public VentanaListarVideo getMiVentanaListarVideo() {
		return miVentanaListarVideo;
	}
	public void setMiVentanaListarVideo(VentanaListarVideo miVentanaListarVideo) {
		this.miVentanaListarVideo = miVentanaListarVideo;
	}
	
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistroVideo() {
		miVentanaRegistroVideo.setVisible(true);
	}
	public void mostrarVentanaConsultaVideo() {
		miVentanaBuscarVideo.setVisible(true);
	}
	public void mostrarVentanaListarVideo() {
		miVentanaListarVideo.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarVideo(Video miVideo) {
		videoServ.validarRegistro(miVideo);
	}
	
	public Video buscarVideo(String miVideo) {
		return videoServ.validarConsulta(miVideo);
	}
	
	public void modificarVideo(Video miCliente) {
		videoServ.validarModificacion(miCliente);
	}
	
	public void eliminarVideo(String codigo) {
		videoServ.validarEliminacion(codigo);
	}
	
	public ArrayList<Video> listarVideos() {
		VideoDao videoDao = new VideoDao();
		return videoDao.listarVideos();
	}


}
