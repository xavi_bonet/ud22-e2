
/*
 * Esta parte del patrón es la que define la lógica de administración del sistema, 
 * establece la conexión entre la vista y el modelo.
 */

package c4.ud22_patronMVC.controller;

import java.util.ArrayList;

import c4.ud22_patronMVC.model.dao.ClienteDao;
import c4.ud22_patronMVC.model.dto.Cliente;
import c4.ud22_patronMVC.model.service.ClienteServ;
import c4.ud22_patronMVC.view.VentanaBuscar;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.VentanaRegistro;
import c4.ud22_patronMVC.view.VentanaListar;


public class ClienteController {
	
	private ClienteServ clienteServ;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistro miVentanaRegistro;
	private VentanaBuscar miVentanaBuscar;
	private VentanaListar miVentanaListar;
	
	//Metodos getter Setters de vistas
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public VentanaRegistro getMiVentanaRegistro() {
		return miVentanaRegistro;
	}
	public void setMiVentanaRegistro(VentanaRegistro miVentanaRegistro) {
		this.miVentanaRegistro = miVentanaRegistro;
	}
	public VentanaBuscar getMiVentanaBuscar() {
		return miVentanaBuscar;
	}
	public void setMiVentanaBuscar(VentanaBuscar miVentanaBuscar) {
		this.miVentanaBuscar = miVentanaBuscar;
	}
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}
	public VentanaListar getMiVentanaListar() {
		return miVentanaListar;
	}
	public void setMiVentanaListar(VentanaListar miVentanaListar) {
		this.miVentanaListar = miVentanaListar;
	}
	
	
	
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistro() {
		miVentanaRegistro.setVisible(true);
	}
	public void mostrarVentanaConsulta() {
		miVentanaBuscar.setVisible(true);
	}
	public void mostrarVentanaListar() {
		miVentanaListar.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCliente(Cliente miCliente) {
		clienteServ.validarRegistro(miCliente);
	}
	
	public Cliente buscarCliente(String codigoCliente) {
		return clienteServ.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Cliente miCliente) {
		clienteServ.validarModificacion(miCliente);
	}
	
	public void eliminarCliente(String codigo) {
		clienteServ.validarEliminacion(codigo);
	}
	
	public ArrayList<Cliente> listarClientes() {
		ClienteDao miClienteDao = new ClienteDao();
		return miClienteDao.listarClientes();
	}


}
