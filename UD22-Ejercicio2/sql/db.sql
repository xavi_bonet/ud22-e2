CREATE DATABASE IF NOT EXISTS `UD22-E2`;
USE `UD22-E2`;

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `cliente`(nombre, apellido, direccion, dni, fecha) VALUES ('Xavier', 'Bonet','c/ cervantes 89','47938829','2021-03-04');

CREATE TABLE `videos` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(250) DEFAULT NULL,
  `director` varchar(250) DEFAULT NULL,
  `cli_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `videos_fk` FOREIGN KEY (`cli_id`) REFERENCES `cliente` (`id`)
);

INSERT INTO `videos`(title, director, cli_id) VALUES ('El bucle infinito', 'lorem ipsum','1');

